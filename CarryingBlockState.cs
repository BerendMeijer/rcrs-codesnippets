﻿using UnityEngine;
using System.Collections;

/// <summary>
/// State for racecars that are carrying blocks or other interactables.
/// </summary>
public class CarryingBlockState : RaceState
{
    private float motorTorqueAtEnteringState;
    public override void Initialize(CarInteractions interactionsController)
    {
        // Get all components from the interactionsController.
        this.interactionsController = interactionsController;
        this.boxCollider = interactionsController.GetComponent<BoxCollider>();
        this.rigidBody = interactionsController.GetComponent<Rigidbody>();
    }

    public override void EnterState()
    {
        motorTorqueAtEnteringState = interactionsController.GetComponent<VehicleController>().motorTorque;
        // Set motortorque to half its value to fake a heavy block.
        interactionsController.GetComponent<VehicleController>().motorTorque *= 0.5f;
    }

    public override void UpdateState(Transform transform)
    {
        // Update the transform so we can work with it.
        this.transform = transform;

        // Detach on button press.
        if (interactionsController.Device.Action1.WasPressed)
        {
            Detach();
        }

     }

    public override void Detach()
    {
        // Using a foreach loop in case more than one block accidentally got connected.
        foreach (Transform block in this.transform)
        {
            // Wait for a given amount of time before releasing a box.
            if (block.tag.Equals("Box"))
            {
                // The block will take care of releasing from the racercar and adding a rigidbody again.
                block.GetComponent<InteractableInfo>().IsParentedInPlayer = false;

                // Register the time the box was dropped so we don't accidentally reconnect immediately.
                interactionsController.boxDropTime = Time.time;

                // This racer is no longer carrying a block so the drivingstate can be set to solo again.
                this.interactionsController.ChangeDrivingState(CarInteractions.DrivingState.Solo);
            }
        }
        // Reset the motortorque.
        interactionsController.GetComponent<VehicleController>().motorTorque = motorTorqueAtEnteringState;
        interactionsController.WantsToConnect = false;
    }

    public override void CarConnectionChange() 
    {
        // Nothing happens when a gameobject calls this method when the car is in this state.
    }

    public override CarInteractions.DrivingState GetDrivingState()
    {
        return CarInteractions.DrivingState.CarryingBlock;
    }
}
