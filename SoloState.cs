﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// State class for the solo driving state. 
/// </summary>
public class SoloState : RaceState
{
    // Variables only needed for this state.
    private float maxRotationForConnecting = 360f;
    private Collider closestCollider;
    private InteractableInfo interactableInfo;
    private LayerMask layerToCheckAtConnection;
    public override void Initialize(CarInteractions interactionsController)
    {
        this.interactionsController = interactionsController;
        this.boxCollider = interactionsController.GetComponent<BoxCollider>();
        this.rigidBody = interactionsController.GetComponent<Rigidbody>();
        this.layerToCheckAtConnection = interactionsController.layerToCheckAtConnection;
    }

    public override void EnterState()
    {
        // No statements for this state.
    }

    public override void UpdateState(Transform transform)
    {
        // Update transform.
        this.transform = transform;

        // Allow connection based on input.
        AllowConnectionOnButtonPress();

        // An overlapshere with a given radius for connecting with cars.
        // A seperate overlapsphere is done for car hitcolliders and block hitcolliders because we want diferent radii for connection with different interactables.
        Collider[] carHitColliders = Physics.OverlapSphere(this.transform.position, 10, interactionsController.carInteractables);

        // An overlapshpere with a given radius, for the blocks.
        Collider[] blockHitColliders = Physics.OverlapSphere(this.transform.position, 5, interactionsController.carInteractables);

        if (blockHitColliders.Length > 0)
        {
            // Only blockers and multitypes need to check blocker options.
            switch (interactionsController.CurrentRacerType)
            {
                case CarInteractions.RacerType.Blocker:
                    CheckBlockerOptions(blockHitColliders);
                    break;
                case CarInteractions.RacerType.MultiType:
                    CheckBlockerOptions(blockHitColliders);
                    break;
                default:
                    break;
            }
        }

        if (carHitColliders.Length > 0)
        {
            switch (interactionsController.CurrentRacerType)
            {
                // Only boosters and multitypes need to check booster options.
                case CarInteractions.RacerType.Booster:
                    CheckBoosterOptions(carHitColliders);
                    break;
                case CarInteractions.RacerType.MultiType:
                    CheckBoosterOptions(carHitColliders);
                    break;
                default:
                    break;
            }
        }
    }

    public override void Detach()
    {
        // Do nothing. We are in solo state.
    }

    public override void CarConnectionChange()
    {
        // In this case, another car inserted a reference to the  
        if (interactionsController.ConnectedCar != null)
        {
            interactionsController.carConnectTime = Time.time;
         // Set this state to the steering state.
            interactionsController.ChangeDrivingState(CarInteractions.DrivingState.Duo_Steering);
        }
    }

    public override CarInteractions.DrivingState GetDrivingState()
    {
        return CarInteractions.DrivingState.Solo;
    }


    //--------------------------------------------------------------------------------//
    // Private methods.

    // This method sets the boolean that determines whether cars can connect. A connection light will turn on when it is true.
    private void AllowConnectionOnButtonPress()
    {
        if (interactionsController.Device.Action1.WasPressed)
        {
            // Flip the boolean.
            interactionsController.WantsToConnect = !interactionsController.WantsToConnect;
        }
    }

    // Method that checks for any interactable racers in the hit list.
    private void CheckBoosterOptions(Collider[] hitColliders)
    {
        // Iterate of the list of hitcolliders.
        for (int i = 0; i < hitColliders.Length; i++)
        {

            if (hitColliders[i].gameObject.CompareTag("Racer") && hitColliders[i].gameObject.GetInstanceID() != this.transform.gameObject.GetInstanceID())
            {
                // Is the interactions script from the other player gettable.
                if (hitColliders[i].GetComponent<CarInteractions>())
                {
                    CarInteractions otherCarInteractionsController = hitColliders[i].GetComponent<CarInteractions>();

                    // The booster can only connect to a racer or a mulitype if they are driving solo.
                    // A booster can connect with ANY type right now.
                    
                    if (otherCarInteractionsController.GetCurrentDrivingState.Equals(CarInteractions.DrivingState.Solo) 
                        && otherCarInteractionsController.CurrentTeamColor == this.interactionsController.CurrentTeamColor 
                        && otherCarInteractionsController.WantsToConnect)
                    {
                        Quaternion otherYrotation = Quaternion.Euler(new Vector3(0, hitColliders[i].transform.eulerAngles.y));
                        Quaternion thisYrotation = Quaternion.Euler(new Vector3(0, this.transform.eulerAngles.y, 0));

                        // Check if the difference in Y rotation is small enough to connect AND action button is pressed.
                        // We are also checking for objects between the two cars. 
                        if (Quaternion.Angle(otherYrotation, thisYrotation) < maxRotationForConnecting && this.interactionsController.WantsToConnect && !CheckForObjectsBetweenConnectees(hitColliders[i].transform))
                        {
                            if ((Time.time - interactionsController.carDisconnectTime > carConnectDisconnectWaitTime) && this.ThisCarIsBooster(otherCarInteractionsController))
                            {
                                // Store the time we connected.
                               interactionsController.carConnectTime = Time.time;

                                // Store a reference to the connected car.
                               interactionsController.ConnectedCar = hitColliders[i].transform;

                                // Parent this car in the other car and place this car directly behind the other racecar.
                               //this.transform.parent = interactionsController.ConnectedCar.transform;

                                Vector3 carDisplacement = new Vector3(0f, 0f, -2f);
                                Vector3 globalDisplacementPoint = interactionsController.ConnectedCar.position + interactionsController.ConnectedCar.transform.rotation * carDisplacement;
                                this.transform.localPosition = globalDisplacementPoint;

                                // Set its rotation towards the same rotation.
                                this.transform.localEulerAngles = interactionsController.ConnectedCar.localEulerAngles;

                                // Notify the other racecar, there is a change in carconnection.
                                interactionsController.ConnectedCar.GetComponent<CarInteractions>().ConnectedCar = this.transform;
                                interactionsController.ConnectedCar.GetComponent<CarInteractions>().CarConnectionChange();

                                // Set states so this racer can start pushing.
                                this.interactionsController.ChangeDrivingState(CarInteractions.DrivingState.Duo_Pushing);
                            }
                        }
                        else
                        {
                            // I am leaving this commented so I can use it later for debugging. 
                            // Debug.Log("Rotation difference is -> " + Quaternion.Angle(otherYrotation, thisYrotation) + " degrees difference");
                        }
                    }
                }
            }
        }
    }

    // Method that checks for boxes the blocker can interact with.
    private void CheckBlockerOptions(Collider[] hitColliders)
    {
        bool boxInFrontOfCar = false;
        //Iterate over the list of hitcolliders.
        for (int i = 0; i < hitColliders.Length; i++)
        {
            // If there is a box nearby and this racer is not carrying a block already and is pressing the action key.
            if (hitColliders[i].CompareTag("Box"))
            {
                // First check if there is a box directly in front of the car (within 4 Unity metres). If there is, we want to pick that one up.

                // Setup the ray. 
                ray.origin = this.transform.position;
                ray.direction = transform.forward * 4;

                RaycastHit[] hits;
                // Store all the raycast hits in an array, raycast all goes through objects.
                hits = Physics.RaycastAll(ray, 4);
                
                foreach(RaycastHit hit in hits)
                {
                    if(hit.transform.CompareTag("Box"))
                    {
                        closestCollider = hit.transform.GetComponent<BoxCollider>();
                        interactableInfo = closestCollider.GetComponent<InteractableInfo>();
                        boxInFrontOfCar = true;
                    }
                }

                if(!boxInFrontOfCar)
                {
                    // If there wasn't a box directly in front of the car, get the closest collider.
                    closestCollider = ReturnClosestCollider(hitColliders, "Box");
                    interactableInfo = closestCollider.GetComponent<InteractableInfo>();
                }

                // Pickup a block if the connection light is on and if there are no objects between the player and the object.
                if (interactionsController.WantsToConnect && RacerCanPickupBlock() && !interactableInfo.IsInDirectSight(this.transform))
                {
                    interactableInfo.ParentTransform = this.transform;
                    interactableInfo.IsParentedInPlayer = true;
                    interactableInfo.SetInterActableColorOnTeamColor(this.interactionsController.CurrentTeamColor);
                    interactionsController.boxPickupTime = Time.time;

                    this.interactionsController.ChangeDrivingState(CarInteractions.DrivingState.CarryingBlock);

                    // Break out of the loop when this racer has connected to a block, to prevent it from connecting to another one.
                    break;
                }
            }
        }
    }

    private bool CheckForObjectsBetweenConnectees(Transform objectToConnectTo)
    {
        // Draw a linecast between this car and the car we want to connect to.
        return Physics.Linecast(this.transform.position, objectToConnectTo.transform.position, out hit, interactionsController.layerToCheckAtConnection);
    }

    /// <summary>
    /// Returns the clostest collider to the player. 
    /// </summary>
    /// <param name="hitColliders">The array with colliders. </param>
    /// <param name="tag"> If left blank it uses all the collider with the default tag. </param>
    /// <returns></returns>
    private Collider ReturnClosestCollider(Collider[] hitColliders, string tag = "default")
    {
        // This method adds each box to a sorted list, sorting by distance.
        SortedList<float, Collider> colliderDistanceList = new SortedList<float, Collider>();
        colliderDistanceList.Clear();

        foreach (Collider hitcollider in hitColliders)
        {
            if (hitcollider.CompareTag(tag))
            {
                float distance = Vector3.Distance(this.transform.position, hitcollider.transform.position);
                if (!colliderDistanceList.ContainsKey(distance))
                {
                    colliderDistanceList.Add(distance, hitcollider);
                }
            }
        }
        // Returns the closest collider, a sortedlist sorts as you add items.
        return colliderDistanceList.Values[0];
    }

    private bool RacerCanPickupBlock()
    {
        // If the B button is pressed and we haven't released a box a really short time ago, the block isn't parented in the player and belongs to this racers team or doesnt belong to any team, the racer can pick it up!

        return (Time.time - interactionsController.boxDropTime > blockPickupReleaseWaitTime &&
               !interactableInfo.IsParentedInPlayer &&
               (interactableInfo.InteractableClaimedByTeam.Equals(this.interactionsController.CurrentTeamColor) || interactableInfo.InteractableClaimedByTeam.Equals(CarInteractions.TeamColor.None)));
    }

    /// <summary>
    /// Checks if this car should be the booster if two cars can connect.
    /// </summary>
    /// <param name="otherCarInteractionsController"></param>
    /// <returns> True if this car should be the booster. </returns>
    private bool ThisCarIsBooster(CarInteractions otherCarInteractionsController)
    {
        Vector3 frontDisplacement = new Vector3(0f, 0f, 1f);
        Vector3 backDisplacement = new Vector3(0f, 0f,- 1f);
 
        // Calculate the global points of the front and back of the cars.
        Vector3 thisBackPosition = this.transform.position + this.transform.rotation * backDisplacement;
        Vector3 thisFrontPosition = this.transform.position + this.transform.rotation * frontDisplacement;

        Vector3 otherFrontPosition = otherCarInteractionsController.transform.position + otherCarInteractionsController.transform.rotation * frontDisplacement;
        Vector3 otherBackPosition = otherCarInteractionsController.transform.position + otherCarInteractionsController.transform.rotation * backDisplacement;

        // If the distance of my front to their back is smaller than their front to my back, I am the booster.
        if (Vector3.Distance(thisFrontPosition, otherBackPosition) < Vector3.Distance(otherFrontPosition, thisBackPosition))
        {
            return true;
        }
        else
        {
            // Else return false; the other car will execute the same function and will connect as booster.
            return false;
        }
    }
}
