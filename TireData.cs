﻿using UnityEngine;
using System.Collections;

public class TireData : MonoBehaviour
{
    // Public variables.
    public bool IsGrounded;
    public bool IsTractionWheel;
    public bool IsSteeringWheel;

    [HideInInspector]
    public float Compression;
    [HideInInspector]
    public float CompressionOld;
    [HideInInspector]
    public Vector3 WheelPosition;
    [HideInInspector]
    public bool HasSkidmark = false;

    // Private variables.
    [SerializeField]
    private float skidmarkSpeed;
    private Vector3 localWheelVelocity;


    private void Update()
    {
        // Only apply skidmarks at the backwheel. Left commented for debugging reasons.
        //if (this.isTractionWheel)
        //{
        // If the tire's sideways velocity is higher than the threshold, spawn a skidmark at its position.
        if (Mathf.Abs(this.localWheelVelocity.x) >= skidmarkSpeed && HasSkidmark == false && IsGrounded)
        {
            SkidmarkController.Spawn(WheelPosition, Quaternion.identity, this.transform);
            HasSkidmark = true;
        }
        // Release skidmark, it will set itself inactive and can be used again.
        else if (HasSkidmark && (Mathf.Abs(this.localWheelVelocity.x) < skidmarkSpeed || !IsGrounded))
        {
            ReleaseSkidmark();
        }
        //}
    }

    // Wheel velocity is set in the VehicleController using the GetPointVelocity method of its Rigidbody.
    public void setWheelVelocity(Vector3 velocity)
    {
        localWheelVelocity = velocity;
    }
    public void setWheelPosition(Vector3 position)
    {
        WheelPosition = position;
    }

    #region Velocity getters
    public float getWheelSidewaysVelocity()
    {
        return getWheelVelocity().x;
    }
    public float getForwardWheelVelocity()
    {
        return getWheelVelocity().z;
    }
    public Vector3 getWheelVelocity()
    {
        return localWheelVelocity;
    }
    #endregion


    public void ReleaseSkidmark()
    {
        // Using a foreach loop to ensure every skidmark is released, if more than one are accidentally added.
        foreach (Transform skidmark in this.transform)
        {
            skidmark.transform.parent = GameObject.FindWithTag("SkidmarksCollection").transform;
            skidmark.GetComponent<SkidmarkController>().SetInActive();
        }
        HasSkidmark = false;
    }
}
