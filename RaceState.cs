﻿using UnityEngine;
using System.Collections;
using InControl;

public abstract class RaceState : MonoBehaviour
{
    // Flag used to only execute functions when the state is enabled.
    public bool StateEnabled = false;

    //-- Protected variables --/

    protected float blockPickupReleaseWaitTime = 0.2f;
    protected float carConnectDisconnectWaitTime = 0.5f;
    protected ParticleSystem smokeparticleSystem;
    protected Rigidbody rigidBody;
    protected BoxCollider boxCollider;
    protected SphereCollider sphereCollider;
    protected Transform transform;
    protected CarInteractions interactionsController;
    protected Ray ray;
    protected RaycastHit hit;
    

    // Methods that must be implemented by deriving classes.
    public abstract void Initialize(CarInteractions interactionsController);
    public abstract void EnterState();
    public abstract void UpdateState(Transform transform);
    public abstract void Detach();
    public abstract void CarConnectionChange();
    public abstract CarInteractions.DrivingState GetDrivingState();
    

}
