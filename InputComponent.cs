﻿using UnityEngine;
using System.Collections;
using InControl;

[RequireComponent (typeof(VehicleController))]
[RequireComponent (typeof(CarInteractions))]

/// <summary>
/// Takes care of getting an inputdevice and giving it to scripts that need one.
/// Also releases assigned inputdevices on destroy.
/// </summary>
public class InputComponent : MonoBehaviour
{
    /// <summary>
    /// An unique ID that can be set at spawning. 
    /// Racecars use this to register with their assigner inputdevice at the inputassigner and retreive their original inputdevice on respawn.
    /// </summary>
    public int inputID = -1;

    [SerializeField]
    private bool debugging;

    private InputDevice inputDevice;
    private bool applicationIsQuitting = false;


    private void Start()
    {
        if(inputID == -1)
        {
            Debug.LogWarning("Dont spawn cars without setting inputIDs! Car is being destroyed automatically to prevent bugs.");
            Destroy(this.transform.parent.gameObject);
            return;
        }

        // Always try to get a inputdevice with the ID assigned.
        // We don't know if this is the first time this car is getting spawned with this ID, the Inputassigner does know!
        GetInputDevicebyID();

        if (inputID != -1)
        {
            GetInputDevice();
        }
    }

    private void OnEnable()
    {
        // Subscribe to the event of new inputdevices coming available if this script didn't get a device on awake.  
        if (!inputDevice)
        {
            InputAssigner.NewDeviceAvailable += this.GetInputDevice;
        }
     }

    private void OnDisable()
    {
        // Cancel the subscription on the event to prevent buggy behavior. 
        InputAssigner.NewDeviceAvailable -= this.GetInputDevice;
    }

    private void OnDestroy()
    {
        // If there is a (controller)device assigned and the game object is destroyed, give it back to the inputassigner so it can be used again.
        // You don't want to call InputAssigner when the application is quitting. 
        // It could be that it was already destroyed and calling Inputassigner will instantiate a new Inputassigner.
        // This could lead to memory(cleanup) leaks when unity is automatically cleaning up the scene.
        if (inputDevice && !applicationIsQuitting)
        {
            InputAssigner.Instance.ReleaseInputDevice(inputDevice);
        }
    }

    private void OnApplicationQuit()
    {
        applicationIsQuitting = true;
    }

    private void GetInputDevicebyID()
    {
        // If there is an unique ID given. 
        if (inputID != -1)
        {
            inputDevice = InputAssigner.Instance.GetInputDeviceByID(this.inputID);
        }

        if (this.inputDevice.Name.Equals("EmptyDevice"))
        {
            Debug.Log("It failed! And got an empty device");
        }
        else
        {
            Debug.Log("It succeeded and got an: " + this.inputDevice.Name.ToString());
            this.GetComponent<VehicleController>().Device = inputDevice;
            this.GetComponent<CarInteractions>().Device = inputDevice;
        }
    }

    /// <summary>
    /// Gets an InputDevice for the scripts on this gameobject.
    /// </summary>
    private void GetInputDevice()
    {
        // Try and get a controller if getting one with ID failed. 
        // This fails in the case of first spawn, when controllers are assigned for the first time.
       
        if (this.inputDevice == null || inputDevice.Name.Equals("EmptyDevice"))
        {
            // Try to get a device by registering with an ID.
            InputDevice device = InputAssigner.Instance.GetNewInputDevice(inputID);

            if (device != null)
            {
                inputDevice = device;

                if (debugging)
                {
                    // Debug can be deleted in the next few weeks. 23/10/2015
                    Debug.Log(this.gameObject.name + " with ID " + this.gameObject.GetInstanceID() + " --> Got an inputdevice with name: " + this.inputDevice.Name.ToString());
                }
                this.GetComponent<VehicleController>().Device = device;
                this.GetComponent<CarInteractions>().Device = device;

                // Keep subscribed to new devices coming available if it got an empty device.
                if (!device.Name.Equals("EmptyDevice"))
                {
                    InputAssigner.NewDeviceAvailable -= this.GetInputDevice;
                    InputManager.OnDeviceDetached += this.CheckDeviceConnection;
                }
            }
            else
            {
                if (debugging)
                {
                    Debug.Log(this.gameObject.name + " with ID " + this.gameObject.GetInstanceID() + " --> Couldnt get a device, still waiting for one.");
                }
            }
        } 
    }

    // Check if this device is disconnected when the "OnDeviceDetached" event is called.
    // The inputassigner itself removes the detached device automatically on this event call.
    private void CheckDeviceConnection(InputDevice detachedDevice)
    {
        if (detachedDevice == this.inputDevice)
        {
            this.inputDevice = null;
            InputAssigner.NewDeviceAvailable += this.GetInputDevice;
            
            // Try to get a new inputdevice immediately. 
            GetInputDevice();
        }
    }
}
