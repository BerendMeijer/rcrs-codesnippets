﻿using UnityEngine;
using System.Linq;  
using System.Collections;
using InControl;
using System.Collections.Generic;

public class InputAssigner : MonoBehaviour
{
    // Public variables. 
    public delegate void OnNewDeviceAvailable();
    // An event that notifies gameobjects when there is a new device available.
    public static event OnNewDeviceAvailable NewDeviceAvailable;

    // Private variables. 
    private static InputAssigner instance = null;

    // Two dictionaries to hold all used devices and unused devices. ID's are stored together with the controller reference.
    private static Dictionary<InputDevice, int> usedDevices = new Dictionary<InputDevice, int>();
    private static Dictionary<InputDevice, int> unusedDevices = new Dictionary<InputDevice, int>();

    //-----------------------------------------------------------------------------------//
    // Singleton functions.

    // C# property to retrieve currently active instance of object.
    public static InputAssigner Instance
    {
        get
        {
            // Lazy instantiate an InputAssigner if it is needed.
            if (instance == null) instance = new GameObject("InputAssigner").AddComponent<InputAssigner>();
            return instance;
        }
    }

    //-----------------------------------------------------------------------------------//
    // Getters and setters.

    /// <summary>
    /// Returns an input device from a list of input devices that aren't assigned yet. 
    /// An input device is added to the list once a button was pressed on the device. 
    /// </summary>
    /// <returns> The latest input device that joined. </returns>
    public InputDevice GetNewInputDevice(int registrationID)
    {
        // Return an inputdevice that does nothing if there is none available.
        InputDevice inputDevice = new InputDevice("EmptyDevice");
                
            // If there is a device that is connected but not yet assigned, we can return it. 
            if (unusedDevices.Count > 0 && unusedDevices.ContainsValue(-1))
            {
                // Return an unused device that has got a default ID of -1.
                inputDevice = unusedDevices.FirstOrDefault(x => x.Value == -1).Key;
                
                // Put it on the list of used devices, remove it from the list unused devices.
                unusedDevices.Remove(inputDevice);
                usedDevices.Add(inputDevice, registrationID);
            }

        return inputDevice;
    }

    /// <summary>
    /// Returns the inputdevice with the unique ID, if there is one. 
    /// </summary>
    /// <param name="ID"></param>
    /// <returns></returns>
    public InputDevice GetInputDeviceByID(int ID)
    {
        InputDevice inputDevice = new InputDevice("EmptyDevice");

        if(unusedDevices.ContainsValue(ID))
        {
            Debug.Log("Found an inputdevice with the ID.");
            
            // Gets the inputdevice (key) by value (ID).
            inputDevice = (unusedDevices.FirstOrDefault (x => x.Value == ID).Key);
            // Move it to the used devices list.    
            unusedDevices.Remove(inputDevice);
            usedDevices.Add(inputDevice, ID);
        }
        return inputDevice;
    }

    /// <summary>
    /// Returns the last active device that pressed a button. Use this when you want all connected controllers to take control over something and call it in an Update.
    /// Returns null if no buttons were pressed on any device since level load.
    /// </summary>
    /// <returns></returns>
    public InputDevice GetLastActiveInputDevice()
    {
        return InputManager.ActiveDevice;
    }

    /// <summary>
    /// Release an uniquely assigned inputdevice from a gameobject when it is no longer needed. 
    /// This prevents controllers and other inputdevices from becoming uselessly assigned to gameobjects that don't exist anymore, or don't need it anymore.
    /// </summary>
    /// <param name="device"> The device that needs to be released. </param>
    public void ReleaseInputDevice(InputDevice device)
    {
        // Move the inputdevice to the list of unused devices if we found one.
        if (usedDevices.ContainsKey(device))
        {
            unusedDevices.Add(device, usedDevices[device]);
            usedDevices.Remove(device);
        }
    }

    //-----------------------------------------------------------------------------------//
    // Monobehavior methods.

    private void Awake()
    {
        #region Singleton checking if there is another instance, cleaning up if there is.

        // Check if there is an existing instance of this object.
        // Only delete a singleton if there isn't anything stored in the lists of devices.
        if ((instance) && (instance.GetInstanceID() != this.GetInstanceID()) && (usedDevices.Count == 0 || unusedDevices.Count == 0))
            // Delete the duplicate.
            DestroyImmediate(gameObject); 
        else
        {   // Make this object the only instance.
            instance = this;
        }
        #endregion
    }

    private void OnEnable()
    {
        // Add this assigner as listener on this event. When a device is being detached, we want to remove it from our internal list of used devices. 
        InputManager.OnDeviceDetached += OnDeviceDetached;
    }

    private void OnDisable()
    {
        InputManager.OnDeviceDetached -= OnDeviceDetached;
    }
     

   private void Update()
    {
        // We constantly need to check if a button was pressed on the last active device and check if it is a device that we already know.
        // The InControl documentation states that this is the only safe way to assign controllers. 
        InputDevice inputDevice = InputManager.ActiveDevice;
        if (AnyButtonWasPressedOnDevice(inputDevice) && !DeviceIsAlreadyKnown(inputDevice))
        {
            // Inputdevices have a default ID of -1.
            unusedDevices.Add(inputDevice, -1);

            // Notify GameObjects there is a new device available, if there are any subscribers.
            if (NewDeviceAvailable != null)
            {
                NewDeviceAvailable();
            }
        }
    }


   private void OnGUI()
   {
       GUI.Label(new Rect(10, 10, 500, 20), "unusedDevices: " + unusedDevices.Count);
       GUI.Label(new Rect(10, 40, 500, 20), "usedDevices: " + usedDevices.Count);
   }


    //----------------------------------------------------------------------------------// 



    //----------------------------------------------------------------------------------//
    // Private methods.

    private void OnDeviceDetached(InputDevice inputDevice)
    {
        if (inputDevice != null)
        {
            RemoveInputDevice(inputDevice);
        }
    }

    // Remove from the list of active or inactive devices.
    private void RemoveInputDevice(InputDevice device)
    {
        if (usedDevices.ContainsKey(device))
        {
            // We have found our device.
            usedDevices.Remove(device);
            // If we have found our device, we can jump out of this method. Devices can't be in the used and unused device lists at the same time. 
            return;
        }

        // If we havent found the device in the used devicelist, search for it in the inActive devicelist. 
        if (unusedDevices.ContainsKey(device))
        {
            unusedDevices.Remove(device);
        }
    }

    private bool AnyButtonWasPressedOnDevice(InputDevice inputDevice)
    {
        return inputDevice.AnyButton || inputDevice.RightBumper || inputDevice.LeftBumper || inputDevice.LeftStick || inputDevice.RightStick;
    }

    // Check if we already have this device in one of our lists.
    // This way, we know if it is a new device, and can assign it to a player. 
    private bool DeviceIsAlreadyKnown(InputDevice inputDevice)
    {
        if (unusedDevices.ContainsKey(inputDevice))
        { 
            return true;
        }

        if (usedDevices.ContainsKey(inputDevice))
        {
            return true;
        }

        // Return false if both lists didn't contain the device.
        return false;
    }
}
