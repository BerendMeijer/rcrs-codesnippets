﻿using UnityEngine;
using System.Collections;

public class DuoPushingState : RaceState
{
    [SerializeField]
    private Material inactiveMeshMaterial;
    [SerializeField]
    private Material activeMeshMaterial;
    [SerializeField]
    private PhysicMaterial dynamicMeshPhysicMaterial;
    [SerializeField]
    private float maxRopeLenght;

    private VehicleController parentVehicleController;
    private Rigidbody connectedCarRigidBody;
    private int boostPower = 10000;
    private int connectedCarInstanceID;

    private Vector3 carDisplacement = new Vector3(0f, 0f, -3f);
    Vector3[] DynamicCarmeshVertices = new Vector3[8];
   
    // For each side, we need 2 triangles. So 4 sides, times 2 triangles.
    // Each triangle has 3 vertices.
    int[] MeshTriangles = new int[24];
    // Dynamic mesh properties.
    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;
    private Mesh mesh;
    private MeshCollider meshCollider;
    private bool detaching = false;
    private float parentMotorTorqueAtEnteringState;

    Vector3 BackPosition = Vector3.zero;
    Vector3 FrontPosition = Vector3.zero;
    Vector3 BeamPosition = Vector3.zero;
    
    public override void Initialize(CarInteractions interactionsController)
    {
        // Get all components from the interactionsController.
        this.interactionsController = interactionsController;
        this.sphereCollider = interactionsController.GetComponentInChildren<SphereCollider>();
        this.boxCollider = interactionsController.GetComponent<BoxCollider>();
        this.rigidBody = interactionsController.GetComponent<Rigidbody>();
        this.smokeparticleSystem = interactionsController.smokeParticle.GetComponent<ParticleSystem>();
        smokeparticleSystem.enableEmission = false;
    }

    /// <summary>
    /// Statements that should only be excecuted once, when entering the state.
    /// </summary>
    public override void EnterState()
    {
        // Cache components.
        parentVehicleController = interactionsController.ConnectedCar.GetComponent<VehicleController>();
        connectedCarRigidBody = interactionsController.ConnectedCar.GetComponent<Rigidbody>();

        parentMotorTorqueAtEnteringState = parentVehicleController.motorTorque;

        this.boxCollider.size = new Vector3(boxCollider.size.x, boxCollider.size.y, boxCollider.size.z / 2);

        // Reset the car displacement point.
        carDisplacement = new Vector3(0f, 0f, -3f);

        // Disable the sphere collider that is in front of the car while connection.
        this.sphereCollider.enabled = false;
        detaching = false;

        // Gets or creates all components that are needed for the dynamic mesh for inbetween the car.
        SetupDynamicMesh();
    }

    public override void UpdateState(Transform transform)
    {
        this.transform = transform;
       
        // Moves the rigidbody to the displacement point, right behind the steering car.
        MoveToPointOnNoCollision();

        // Boost on input, also enables smoke particles.
        BoostOnInput();

        // Detaches from other car on input.
        DetachOnInput();

        // Moves the global displacement point backwards on input.
        StretchRopeOnInput();

        // If the cars are not in the middle of detaching, to prevent null references.
        if (!detaching)
        {
            // Creates a new mesh inbetween connected cars, the new mesh is a trigger.
            CreateNewMesh();
        }
        if (!detaching)
        {
            if (carDisplacement.z < -3.5f)
            {
                parentVehicleController.motorTorque = parentMotorTorqueAtEnteringState / 2;
            }
            else
            {
                parentVehicleController.motorTorque = parentMotorTorqueAtEnteringState;
            }
        }

    }


    public override void Detach()
    {
        // Check if there is a car connected.
        if (this.interactionsController.ConnectedCar != null)
        {
            interactionsController.carDisconnectTime = Time.time;

            // Set the boxdroptime so cars don't connect immediately to blocks when they disconnect.
            interactionsController.boxDropTime = Time.time;

            // Set rigidbodies to non kinematic if they were  set to kinematic during connection.
            interactionsController.GetComponent<Rigidbody>().isKinematic = false;
            interactionsController.ConnectedCar.GetComponent<Rigidbody>().isKinematic = false;

            // Set the connectedbody (our body) to null.
            this.interactionsController.ConnectedCar.GetComponent<CarInteractions>().ConnectedCar = null;

            // Notify the other racecar that something changed in our connection. 
            this.interactionsController.ConnectedCar.GetComponent<CarInteractions>().CarConnectionChange();

            smokeparticleSystem.enableEmission = false;

            // No car should be connected anymore, so set connected car to null.
            interactionsController.ConnectedCar = null;

            // Rescale boxcollider to its original size.
            this.boxCollider.size = new Vector3(boxCollider.size.x, boxCollider.size.y, boxCollider.size.z * 2);

            // Turn the spherecollider on again.
            this.sphereCollider.enabled = true;

            this.interactionsController.WantsToConnect = false;

            // Destroy the dynamic mesh components attached to this gameobject.
            DestroyDynamicMesh();

            this.interactionsController.ChangeDrivingState(CarInteractions.DrivingState.Solo);
        }
    }

    public override void CarConnectionChange()
    {
        // If the pushing car removed itself as connected car, it should be null.
        if (this.interactionsController.ConnectedCar == null)
        {
            interactionsController.carDisconnectTime = Time.time;
            // Set the boxdroptime so cars don't connect immediately to blocks when they disconnect.
            interactionsController.boxDropTime = Time.time;
            this.boxCollider.size = new Vector3(boxCollider.size.x, boxCollider.size.y, boxCollider.size.z * 2);
            this.sphereCollider.enabled = true;
            smokeparticleSystem.enableEmission = false;

            // Destroy the dynamic mesh components attached to this gameobject.
            DestroyDynamicMesh();

            interactionsController.GetComponent<Rigidbody>().isKinematic = false;

            interactionsController.ChangeDrivingState(CarInteractions.DrivingState.Solo);
        }
    }

    public override CarInteractions.DrivingState GetDrivingState()
    {
        return CarInteractions.DrivingState.Duo_Pushing;
    }
   
   
    private void StretchRopeOnInput()
    {
        // Stretching the rope on a single button press.
        if (interactionsController.Device.Action3.WasPressed)
        {
            if (carDisplacement.z == maxRopeLenght)
            {
                // Shrink the rope if its length is already at is
                carDisplacement = new Vector3(carDisplacement.x, carDisplacement.y, -3);
            }
            else
            {
                carDisplacement = new Vector3(carDisplacement.x, carDisplacement.y, maxRopeLenght);
            }
        }
        
    }

    /// <summary>
    /// Calculates all the points based on the box colliders of the two cars.
    /// Draws a mesh inbetween these points.
    /// </summary>
    private void CreateNewMesh()
    {
        // Rope mechanic between cars.
        mesh.Clear();

        // Step 1. Get the 4 points in space of the pushing car (this car), we want to connect a mesh inbetween.
        // Step 2. Get the 4 points in space of the steering car (other car), we want to connect a mesh inbetween.
        // Step 3. Draw a mesh on the top, clockwise.
        // Step 4. Draw a mesh on the bottom, clockwise.
        // Step 5. Draw a mesh on the right side, clockwise.
        // Step 6. Draw a mesh on the left side, clockwise.
        // Step 7. Recalculate normals, UV's, reassign mesh to collider.

        //--------------------------------------------------------------------------------------------------------//
        // Step 1. Get the 4 points in space of the pushing car (this car), we want to connect a mesh inbetween.
        // First get all 4 points on this car that we want the mesh to connect to. 
        // If we were viewing from the back car to the front car, these are the 4 points we want to connect to. 
        float zOffsetToEdge = boxCollider.size.z / 2;
        float xOffsetToEdge = boxCollider.size.x / 2;
        float yOffsetToEdge = boxCollider.size.y / 2;


        // Left top. (0)
        // Left top offset.
        Vector3 PushingCarLeftTopOffset = new Vector3(-xOffsetToEdge, yOffsetToEdge, zOffsetToEdge);
        // Calculate the position of the vertice in world space.
        Vector3 PushingCarLeftTopPositon = this.interactionsController.transform.position + this.interactionsController.transform.rotation * PushingCarLeftTopOffset;

        // Right top. (1)
        Vector3 PushingCarRightTopOffset = new Vector3(xOffsetToEdge, yOffsetToEdge, zOffsetToEdge);
        Vector3 PushingCarRightTopPositon = this.interactionsController.transform.position + this.interactionsController.transform.rotation * PushingCarRightTopOffset;

        // Left bottom. (2)
        Vector3 PushingCarLeftBottomOffset = new Vector3(-xOffsetToEdge, -yOffsetToEdge, zOffsetToEdge);
        Vector3 PushingCarLeftBottomPositon = this.interactionsController.transform.position + this.interactionsController.transform.rotation * PushingCarLeftBottomOffset;

        // Right bottom. (3)
        Vector3 PushingCarRightBottomOffSet = new Vector3(xOffsetToEdge, -yOffsetToEdge, zOffsetToEdge);
        Vector3 PushingCarRightBottomPosition = this.interactionsController.transform.position + this.interactionsController.transform.rotation * PushingCarRightBottomOffSet;

        DynamicCarmeshVertices[0] = interactionsController.transform.InverseTransformPoint(PushingCarLeftTopPositon);
        DynamicCarmeshVertices[1] = interactionsController.transform.InverseTransformPoint(PushingCarRightTopPositon);
        DynamicCarmeshVertices[2] = interactionsController.transform.InverseTransformPoint(PushingCarLeftBottomPositon);
        DynamicCarmeshVertices[3] = interactionsController.transform.InverseTransformPoint(PushingCarRightBottomPosition);

        //-----------------------------------------------------------------------//
        // Step 2. Get the 4 points in space of the steering car (other car), we want to connect a mesh inbetween.

        // Recalculate the box collider offsets in case the other car his boxcollider has a different size.
        zOffsetToEdge = this.interactionsController.ConnectedCar.GetComponent<BoxCollider>().size.z / 2;
        xOffsetToEdge = this.interactionsController.ConnectedCar.GetComponent<BoxCollider>().size.x / 2;
        yOffsetToEdge = this.interactionsController.ConnectedCar.GetComponent<BoxCollider>().size.y / 2;

        // Left top (4)
        // Left top offset
        Vector3 SteeringCarLeftTopOffset = new Vector3(-xOffsetToEdge, yOffsetToEdge, -zOffsetToEdge);
        // Calculate the position of the vertice in world space.
        Vector3 SteeringCarLeftTopPositon = this.interactionsController.ConnectedCar.position + this.interactionsController.ConnectedCar.rotation * SteeringCarLeftTopOffset;

        // Right top (5)
        Vector3 SteeringCarRightTopOffset = new Vector3(xOffsetToEdge, yOffsetToEdge, -zOffsetToEdge);
        Vector3 SteeringCarRightTopPositon = this.interactionsController.ConnectedCar.position + this.interactionsController.ConnectedCar.rotation * SteeringCarRightTopOffset;

        // Left bottom (6)
        Vector3 SteeringCarLeftBottomOffset = new Vector3(-xOffsetToEdge, -yOffsetToEdge, -zOffsetToEdge);
        Vector3 SteeringCarLeftBottomPositon = this.interactionsController.ConnectedCar.position + this.interactionsController.ConnectedCar.rotation * SteeringCarLeftBottomOffset;

        // Right bottom (7)
        Vector3 SteeringCarRightBottomOffSet = new Vector3(xOffsetToEdge, -yOffsetToEdge, -zOffsetToEdge);
        Vector3 SteeringCarRightBottomPosition = this.interactionsController.ConnectedCar.position + this.interactionsController.ConnectedCar.rotation * SteeringCarRightBottomOffSet;

        DynamicCarmeshVertices[4] = interactionsController.transform.InverseTransformPoint(SteeringCarLeftTopPositon);
        DynamicCarmeshVertices[5] = interactionsController.transform.InverseTransformPoint(SteeringCarRightTopPositon);
        DynamicCarmeshVertices[6] = interactionsController.transform.InverseTransformPoint(SteeringCarLeftBottomPositon);
        DynamicCarmeshVertices[7] = interactionsController.transform.InverseTransformPoint(SteeringCarRightBottomPosition);

        // Assign vertices to mesh.
        mesh.vertices = DynamicCarmeshVertices;

        // Step 3. Draw a mesh on the top, clockwise.
        MeshTriangles[0] = 5;
        MeshTriangles[1] = 1;
        MeshTriangles[2] = 0;

        MeshTriangles[3] = 5;
        MeshTriangles[4] = 0;
        MeshTriangles[5] = 4;

        //// Step 4. Draw a mesh on the bottom, clockwise.
        MeshTriangles[6] = 3;
        MeshTriangles[7] = 7;
        MeshTriangles[8] = 2;

        MeshTriangles[9] = 6;
        MeshTriangles[10] = 2;
        MeshTriangles[11] = 7;

        //// Step 5. Draw a mesh on the right side, clockwise.
        MeshTriangles[12] = 5;
        MeshTriangles[13] = 3;
        MeshTriangles[14] = 1;

        MeshTriangles[15] = 5;
        MeshTriangles[16] = 7;
        MeshTriangles[17] = 3;

        //// Step 6. Draw a mesh on the left side, clockwise.
        MeshTriangles[18] = 0;
        MeshTriangles[19] = 2;
        MeshTriangles[20] = 4;

        MeshTriangles[21] = 2;
        MeshTriangles[22] = 6;
        MeshTriangles[23] = 4;

        // Assign triangles to mesh.
        mesh.triangles = MeshTriangles;

        Vector3[] normals = new Vector3[8];

        normals[0] = -Vector3.forward;
        normals[1] = -Vector3.forward;
        normals[2] = -Vector3.forward;
        normals[3] = -Vector3.forward;
        normals[4] = -Vector3.forward;
        normals[5] = -Vector3.forward;
        normals[6] = -Vector3.forward;
        normals[7] = -Vector3.forward;

        mesh.normals = normals;

        // Uncomment and fix this section when using a texture on the mesh.
        Vector2[] uv = new Vector2[8];

        uv[0] = new Vector2(0, 0);
        uv[1] = new Vector2(1, 0);
        uv[2] = new Vector2(0, 1);
        uv[3] = new Vector2(1, 1);
        uv[4] = new Vector2(0, 0);
        uv[5] = new Vector2(1, 0);
        uv[6] = new Vector2(0, 1);
        uv[7] = new Vector2(1, 1);

        mesh.uv = uv;
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        mesh.Optimize();

        meshCollider.sharedMesh = mesh;
    }

    /// <summary>
    /// Destroys all the components that have to do with the dynamic mesh.
    /// Should be called on detach.
    /// </summary>
    private void DestroyDynamicMesh()
    {
        Destroy(meshCollider);
        Destroy(meshRenderer);
        Destroy(meshFilter);
    }

    // Function pushing back any player other than the two connectees when they enter the wall.
    private void OnTriggerStay(Collider other)
    {
        // Check if this state is enabled. There is no way of enabling or disabling the entire script.
        if (StateEnabled)
        {
            if (other.gameObject.CompareTag("Racer"))
            {
                VehicleController otherVehicleController = other.GetComponentInChildren<VehicleController>();
                if (!this.GetComponent<VehicleController>().Equals(otherVehicleController))
                {
                    if (!this.interactionsController.ConnectedCar.GetComponentInChildren<VehicleController>().Equals(otherVehicleController))
                    {

                        // Check the direction the car is facing by checking the difference in distance between the front and the back car.
                        Vector3 frontDisplacement = new Vector3(0f, 0f, 1f);
                        Vector3 backDisplacement = new Vector3(0f, 0f,- 1f);
 
                        // Calculate the global points of the front and back of the cars.
                        BackPosition = other.transform.position + other.transform.rotation * backDisplacement;
                        FrontPosition = other.transform.position + other.transform.rotation * frontDisplacement;
                        BeamPosition = (this.transform.position + this.interactionsController.ConnectedCar.transform.position) / 2;

                        float FrontToBeamDistance = Vector3.Distance(FrontPosition, BeamPosition);
                        float BackToBeamDistance = Vector3.Distance(BackPosition, BeamPosition);

                       
                            // If the front of the car is closer to the beam than the back of the car, the car is face towards the beam.
                            if (FrontToBeamDistance < BackToBeamDistance)
                            {
                                other.GetComponent<Rigidbody>().AddForce(-other.transform.forward * (50000 * Time.deltaTime), ForceMode.Impulse);

                            }
                            else
                            {
                                other.GetComponent<Rigidbody>().AddForce(other.transform.forward * (50000 * Time.deltaTime), ForceMode.Impulse);
                            }
                        Debug.DrawRay(other.transform.position, -other.GetComponent<Rigidbody>().velocity, Color.red, 1);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Moves the rigidbody of this car to the displacement point if there is no collider intersecting the point, and if there is nothing inbetween the connected cars.
    /// </summary>
    private void MoveToPointOnNoCollision()
    {
        // Calculate the global displacement point: target position of racecar behind other racer.
        Vector3 globalDisplacementPoint = interactionsController.ConnectedCar.position + interactionsController.ConnectedCar.transform.rotation * carDisplacement;
        RaycastHit hit;
        
        // If it is intersecting, we don't want to move to it. 
        if (!Physics.CheckSphere(globalDisplacementPoint, 0.1f, interactionsController.layerToCheckAtConnection))
        {
            // Then, check if there is a clear line between the pushing car and steering car. 
            // If there is an object inbetween, we don't want to move to it.
            if (!Physics.Linecast(globalDisplacementPoint, interactionsController.ConnectedCar.position, out hit, interactionsController.layerToCheckAtConnection))
            {
                // There point is clear of colliders and there is a clear line between the car and the connectee.
                // Move towards the point.
                this.rigidBody.MovePosition(globalDisplacementPoint);
                // Rotate to the same rotation as the other vehicle.
                this.rigidBody.MoveRotation(interactionsController.ConnectedCar.rotation);
            }
        } 
        // It did not fit, seek a Z position where we do fit in.
        else
        {
            // Try to find a position where we can fit.
            int maxZDistance = -2;
            for (float distance = carDisplacement.z; distance < maxZDistance; distance += 0.5f)
            {
               // Set car displacement to new value;
              carDisplacement = new Vector3(carDisplacement.x, carDisplacement.y, distance);
              // Recalculate global displacement point.
              globalDisplacementPoint = interactionsController.ConnectedCar.position + interactionsController.ConnectedCar.transform.rotation * carDisplacement;

              //Debug.Log("Trying again at:" + carDisplacement);

              if (!Physics.CheckSphere(globalDisplacementPoint, 0.1f, interactionsController.layerToCheckAtConnection))
              {
                  // Then, check if there is a clear line between the pushing car and steering car. 
                  // If there is an object inbetween, we don't want to move to it.
                  if (!Physics.Linecast(globalDisplacementPoint, interactionsController.ConnectedCar.position, out hit, interactionsController.layerToCheckAtConnection))
                  {
                      // There point is clear of colliders and there is a clear line between the car and the connectee.
                      // Move towards the point.
                      this.rigidBody.MovePosition(globalDisplacementPoint);
                      // Rotate to the same rotation as the other vehicle.
                      this.rigidBody.MoveRotation(interactionsController.ConnectedCar.rotation);
                      // Break out of this loop.
                      break;
                  }
              }
            }
        }
    }

    /// <summary>
    /// Adds force to the steering car on input, and enables the smoke particle for visual feedback.
    /// </summary>
    private void BoostOnInput()
    {
        // Boost with right bumper only when the parent has all wheels connected to the ground and the parent isn't trying to brake or reverse.
        if (interactionsController.Device.RightBumper > 0 && parentVehicleController.AllWheelsAreGrounded && !parentVehicleController.Device.LeftBumper && carDisplacement.z >= -3.1)
        {
            connectedCarRigidBody.AddForce(interactionsController.ConnectedCar.transform.forward * boostPower * Time.deltaTime);
            smokeparticleSystem.enableEmission = true;
        }
        else
        {
            smokeparticleSystem.enableEmission = false;
        }
    }

    /// <summary>
    /// Calls the detach method on a button press.
    /// </summary>
    private void DetachOnInput()
    {
        // Detach with 'A' button.
        if (interactionsController.Device.Action1.WasPressed && (Time.time - interactionsController.carConnectTime > carConnectDisconnectWaitTime))
        {
            detaching = true;
            Detach();
        }
    }

    /// <summary>
    /// Sets up all mesh components that are needed for the dynamic mesh drawed between the two connected cars.
    /// </summary>
    private void SetupDynamicMesh()
    {
        // Get the meshfilter, or add one if there isn't one yet.
        meshFilter = (this.GetComponent<MeshFilter>() ? null : this.gameObject.AddComponent<MeshFilter>());

        // Get the meshrenderer, or add one if there isn't one yet. 
        meshRenderer = (this.GetComponent<MeshRenderer>() ? null : this.gameObject.AddComponent<MeshRenderer>());
        meshCollider = (this.GetComponent<MeshCollider>() ? null : this.gameObject.AddComponent<MeshCollider>());
        meshRenderer.sharedMaterial = inactiveMeshMaterial;

        // Set the mesh.
        mesh = meshFilter.mesh;

        // Configure the meshcollider.
        meshCollider.convex = true;
        meshCollider.isTrigger = true;
        meshCollider.material = dynamicMeshPhysicMaterial;
        meshCollider.sharedMesh = mesh;
    }

}


