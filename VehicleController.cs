﻿using UnityEngine;
using System.Collections;
using InControl;
using System.Collections.Generic;

#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// This class controls the physics of the vehicle and uses user input to add forces.
/// This includes tire traction, 
/// </summary>
/// 
// VehicleController depends on these components.
[RequireComponent(typeof(CarInteractions))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(InputComponent))]
public class VehicleController : MonoBehaviour
{
    // ---------------------------------------------------------------------------------// 
    // Getters and Setters (public).
    public InputDevice Device { get; set; }
    public bool AllWheelsAreGrounded;
    public float motorTorque;

    //  Private variables.
    [SerializeField]
    private bool debugging;
    [SerializeField]
    private bool invertedSteering;

    [Header("Car Physics values")]
    [SerializeField]
    private GameObject[] rayCastOrigins;
    [SerializeField]
    private float springConstant;
    [SerializeField]
    private float dampConstant;
    [SerializeField]
    private float brakingForce;
    [SerializeField]
    private float suspenceLength;
    [SerializeField]
    private float forwardTractionForce;
    [SerializeField]
    private float maxAllowedXRotation;
    [SerializeField]
    private float maxAllowedZRotation;
    [SerializeField]
    private float sidewaysTractionForce;
    [SerializeField]
    private float steerTorque;
    [SerializeField]
    private bool useCompressionToCalculateTraction;

    [Header("Carsettings to use")]
    [SerializeField]
    private CarSettings settings;

    // Variable to store the local rotation.
    private float localxDegrees, localzDegrees, localyDegrees;
    private float lastLocalyDegrees = 0;
    private float currentlocalYdegrees = 0;
    private float horizontalInput, verticalInput, leftTriggerInput, rightTriggerInput;
    private bool AButton, BButton, XButton, YButton;
    private float tempSidewaysTractionForce, tempForwardTractionForce, tempSteeringForce;
    private float tractionCoefficient = 1.0f;
    private float rollingFriction = 1.0f;

    [Header("Traction curves")]
    // Components.
    [SerializeField]
    private AnimationCurve sideWaysTractionCurve;
    [SerializeField]
    private AnimationCurve forwardTractionCurve;
    [SerializeField]
    private AnimationCurve steeringTorqueCurve;

    private Rigidbody body3d;
    private TireData tireData;
    private BoxCollider boxCollider;
    private RaycastHit hit;
    // A local variable used for iterating.
    private GameObject raycastOrigin;
    private TireData[] tireDataComponentArray;


    

    //----------------------------------------------------------------------------------------//
    // Monobehavior methods (private).
    private void Awake()
    {
        body3d = GetComponent<Rigidbody>();
        body3d.centerOfMass = Vector3.zero;
        boxCollider = this.GetComponent<BoxCollider>();

        tempForwardTractionForce = forwardTractionForce;
        tempSidewaysTractionForce = sidewaysTractionForce;
        tempSteeringForce = steerTorque;


        #region If there is a settings asset in the inspector, use it to set the values.
        if (this.settings != null)
        {
            this.springConstant = settings.springConstant;
            this.dampConstant = settings.dampConstant;
            this.motorTorque = settings.motorTorque;
            this.steerTorque = settings.steerTorque;
            this.suspenceLength = settings.suspenceLength;
            this.sidewaysTractionForce = settings.sidewaysTractionForce;
            this.forwardTractionForce = settings.forwardTractionForce;
            this.maxAllowedXRotation = settings.maxAllowedXRotation;
            this.maxAllowedZRotation = settings.maxAllowedZRotation;
            this.useCompressionToCalculateTraction = settings.useCompressionToCalculateTraction;
            this.forwardTractionCurve = settings.forwardTractionCurve;
            this.sideWaysTractionCurve = settings.sidewaysTractionCurve;
            this.steeringTorqueCurve = settings.steerTorqueCurve;
        }
        #endregion

        // The array will be as long as the amount of raycastOrigins we get (amount of wheels).
        // Storing the TireData components in an array will prevent us to do it every update loop.
        tireDataComponentArray = new TireData[rayCastOrigins.Length];
        for (int i = 0; i < rayCastOrigins.Length; i++)
        {
            tireDataComponentArray[i] = rayCastOrigins[i].GetComponent<TireData>();
        }
    }


    private void Start()
    {
        // These methods are in start because they depend on settings set in the awake of VehicleControlle and settings set when instantiating (this is also before Start).
        AdjustMotorTorqueBasedOnType();

        // Scales all the physics values, raycastOrigins and suspension length so the settings convert to a larger or smaller scale.
        ScalePhysicsToBoxCollider();

        StartCoroutine(EnableSphereColliderOnDelay(0.5f));
    }

    #region Draw a debug GUI with vehicle data
    private void OnGUI()
    {
        if (debugging)
        {
            GUI.Label(new Rect(10, 10, 500, 20), "Car velocity:      " + string.Format("{0:00.00}", this.gameObject.GetComponent<Rigidbody>().velocity.magnitude)); //* 3.6f) + " KPH");
            GUI.Label(new Rect(10, 30, 500, 20), "Forward velocity:  " + string.Format("{0:00.00}", transform.InverseTransformDirection(this.gameObject.GetComponent<Rigidbody>().velocity).z)); //* 3.6f) + " KPH");
            GUI.Label(new Rect(10, 50, 500, 20), "Sideways velocity: " + string.Format("{0:00.00}", transform.InverseTransformDirection(this.gameObject.GetComponent<Rigidbody>().velocity).x));// * 3.6f) + " KPH");
            GUI.Label(new Rect(10, 80, 500, 20), "Forward traction curve " + (forwardTractionCurve.Evaluate(transform.InverseTransformDirection(this.gameObject.GetComponent<Rigidbody>().velocity).z / 70)));// * 3.6f) + " KPH");
            GUI.Label(new Rect(10, 110, 500, 20), "Sideways traction curve " + (sideWaysTractionCurve.Evaluate(Mathf.Abs(transform.InverseTransformDirection(this.gameObject.GetComponent<Rigidbody>().velocity).x) / 50)));// * 3.6f) + " KPH");
        }
    }

    private void OnDrawGizmos()
    {
        if (debugging)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(body3d.worldCenterOfMass, .1f);

            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(this.transform.position, 3);

            // Draw a cube behind the car to visualize the connection point.
            Gizmos.color = Color.green;
            Vector3 cubeDisplacement = new Vector3(0f, 0f, -4f);
            Vector3 cubeGlobalPosition = this.transform.position + transform.rotation * cubeDisplacement;
            Gizmos.DrawCube(cubeGlobalPosition, Vector3.one);

        }
    }

    #endregion

    private void Update()
    {
        // If the Y button is pressed, invert the steering.
        if (YButton)
        {
            invertedSteering = true;
        }

        // Setting the inertia tensor to a value that the car has naturally, without being connected to anything. 
        // This needs to be set every frame to prevent it from changing when connecting to a block (since Unity 5.3).
        body3d.inertiaTensor = new Vector3(22.6f, 29.3f, 11.2f);
    }

    private void FixedUpdate()
    {
        // Makes traction and steering torque dependant on your speed.
        CalculateDynamicTraction();

        // Get input values and store them in variables like "horizontalInput", if there is a controller connected.
        CacheInput();

        // Adds all forces to the vehicle, based on input and physics. 
        AddSuspensionAndForces();

        // Calculate local rotation, we use this to constrain the rotation in the air.
        CalculateLocalYRotation();

        // Stop the car from rotating if it is rotating too much.
        ConstrainRotation();

        // Update FMOD rpm values.
        UpdateFMOD();
    }


    // Uncomment this code and "VibrateOnImpact" if you want controller rumble.
    // At the time of writing has a bug with Xbox one controllers, see the VibrateOnImpact function for more info.

    //private void OnCollisionEnter(Collision col)
    //{
    //    // Vibrate if the car hits walls or other cars. 
    //    if (!col.gameObject.CompareTag("Ground"))
    //    {
    //        // Controller vibration.
    //        StartCoroutine(VibrateOnImpact(0.1f, Mathf.Clamp((Mathf.Abs(col.relativeVelocity.magnitude / 50)), 0, 0.5f)));
    //    }
    //}

    private void OnDestroy()
    {
        StopAllCoroutines();
        // Stop the controller from vibrating. 
        Device.StopVibration();
    }




    //-----------------------------------------------------------------------------------------------//
    // Private methods.
    private void AddSuspensionAndForces()
    {
        // Reset this flag before every for loop. If any of the wheels are off the ground, this flag is set to false in the else statement of the Physics raycast.
        this.AllWheelsAreGrounded = true;

        for (int i = 0; i < rayCastOrigins.Length; i++)
        {
            // Save the gameobject we are iterating over in a variable.
            raycastOrigin = rayCastOrigins[i];
            tireData = tireDataComponentArray[i];

            // If there is contact with the ground (suspension hits the ground).
            if (Physics.Raycast(raycastOrigin.transform.position, -transform.up, out hit, suspenceLength))
            {
                SetFrictionOnSurfaceName(hit);

                #region Draw a green ray if the suspension is pressed
                if (debugging)
                {
                    Debug.DrawRay(raycastOrigin.transform.position, -transform.up * suspenceLength, Color.green);
                    raycastOrigin.GetComponent<TextMesh>().text = string.Format("{0:00.00}", (tireData.Compression));
                }
                #endregion

                SetTireDataValues();

                ApplyTraction();

                // Add forces using input.
                AddTorqueAndBrakes();
            }
            else
            {
                //  If there is no contact, draw a Red ray.
                //  Draw the suspension from wheel towards ground.
                if (debugging)
                {
                    Debug.DrawRay(raycastOrigin.transform.position, -transform.up * suspenceLength, Color.red);
                }

                // Tell the tire that it isnt grounded anymore.
                tireData.IsGrounded = false;
                // This means that not all wheels are grounded anymore. 
                this.AllWheelsAreGrounded = false;
            }
        }
    }

    //---------------------------------------------------------------------------------------------//
    // Methods called from Start().
    private void AdjustMotorTorqueBasedOnType()
    {
        switch (this.GetComponent<CarInteractions>().CurrentRacerType)
        {

            case CarInteractions.RacerType.Racer:
                this.motorTorque *= 1.0f;
                break;
            case CarInteractions.RacerType.Blocker:
                this.motorTorque *= 0.9f;
                break;
            case CarInteractions.RacerType.Booster:
                this.motorTorque *= 1.05f;
                break;
            case CarInteractions.RacerType.MultiType:
                this.motorTorque *= 1f;
                break;
            default: Debug.LogError("Car type unknown");
                break;
        }
    }

    // Scales all the physics values, raycastOrigins and suspension length so the settings convert to a larger or smaller scale.
    private void ScalePhysicsToBoxCollider()
    {
        // The new scale we are going to scale to. Original z size was 2! 
        float targetScale = (boxCollider.size.z / 2);
        // The ratio we need to scale our mass physics to, pushing and pulling forces.
        float massRatio = Mathf.Pow(targetScale, 3);
        // Torque needs to be scaled differently to make up for the mass, and the larger scale as well. 
        float torqueRatio = Mathf.Pow(targetScale, 5);
        float xTargetPositionRay = (boxCollider.size.x / 2);
        float zTargetPositionRay = (boxCollider.size.z / 2);


        #region Position the raycasts according to scale
        foreach (GameObject rOrigin in rayCastOrigins)
        {
            switch (rOrigin.gameObject.name)
            {
                case "RaycastOrgin (leftFront)":
                    rOrigin.gameObject.transform.localPosition = new Vector3(-xTargetPositionRay, 0, zTargetPositionRay);
                    break;
                case "RaycastOrgin (rightFront)":
                    rOrigin.gameObject.transform.localPosition = new Vector3(xTargetPositionRay, 0, zTargetPositionRay);
                    break;
                case "RaycastOrgin (rightBack)":
                    rOrigin.gameObject.transform.localPosition = new Vector3(xTargetPositionRay, 0, -zTargetPositionRay);
                    break;
                case "RaycastOrgin (leftBack)":
                    rOrigin.gameObject.transform.localPosition = new Vector3(-xTargetPositionRay, 0, -zTargetPositionRay);
                    break;
                default:
                    Debug.LogError("Couldnt scale the raycastOrigins, the names arent matching");
                    break;
            }
        }
        #endregion
        this.suspenceLength *= targetScale;

        // Scale all the values that have a relation with mass. 
        this.body3d.mass *= massRatio;
        this.motorTorque *= massRatio;
        this.forwardTractionForce *= massRatio;
        this.sidewaysTractionForce *= massRatio;
        this.springConstant *= massRatio;
        this.dampConstant *= massRatio;

        tempForwardTractionForce *= massRatio;
        tempSidewaysTractionForce *= massRatio;
        tempSteeringForce *= torqueRatio;


        // Set values that relate to torque.
        this.steerTorque *= torqueRatio;
    }

    //------------------------------------------------------------------------------------------------------//
    // Methods called from FixedUpdate().

    private void AddTorqueAndBrakes()
    {
        steerTorque = tempSteeringForce * steeringTorqueCurve.Evaluate(transform.InverseTransformDirection(this.gameObject.GetComponent<Rigidbody>().velocity).z / 70);
        #region Add forward and sideways forces to the vehicle

       

        // Capping the maximum Y rotation to 3 degrees per frame to prevent overdrifting and losing control.
        // The ternary operator is used to allow a faster turning speed when the gas pedal is not pressed. 
        if (Mathf.Abs((lastLocalyDegrees - currentlocalYdegrees)) < (rightTriggerInput > 0 ? 2.5f : 3.0f))
        { 
            if (tireData.IsSteeringWheel && body3d.velocity.magnitude > 0.5f)
            {
                // Add torque to the rigidbody according to the amount of horizontal input. If we are having a negative velocity, give back full steering torque.
                // If the car has a forward velocity. 
                if (transform.InverseTransformDirection(this.gameObject.GetComponent<Rigidbody>().velocity).z > 0)
                {
                    body3d.AddTorque(transform.up * steerTorque * (Mathf.Clamp((body3d.velocity.magnitude / 10), 0.2f, 1)) * horizontalInput);
                }
                else
                {
                    // We are driving backwards and inverted steering is enabled, invert the steering.
                    if (invertedSteering)
                    {
                        body3d.AddTorque(transform.up * steerTorque * (Mathf.Clamp((body3d.velocity.magnitude / 10), 0.2f, 1)) * -horizontalInput);
                    }
                    else
                    {
                        body3d.AddTorque(transform.up * steerTorque * (Mathf.Clamp((body3d.velocity.magnitude / 10), 0.2f, 1)) * horizontalInput);
                    }
                }
            }
        }

        // Apply forward force at the body, but only when all wheels are grounded. 
        // This prevents the car from launching in the air. 
        if (tireData.IsTractionWheel && this.AllWheelsAreGrounded)
        {
            // Add forward force to the car on right trigger input.
            body3d.AddForce((transform.forward * (motorTorque * tractionCoefficient)) * rightTriggerInput);
          
            // Add backwards force to the car on left trigger input.
            // If the car is going forward, apply brakes.
            if ((transform.InverseTransformDirection(this.gameObject.GetComponent<Rigidbody>().velocity).z > 1f))
            {
                body3d.AddForce((-transform.forward * (brakingForce * tractionCoefficient) * 
                Mathf.Clamp(1 - ((transform.InverseTransformDirection(this.gameObject.GetComponent<Rigidbody>().velocity).z) / 80.0f), 0, 1) *leftTriggerInput) * 
                Mathf.Clamp((1/(Mathf.Abs(body3d.angularVelocity.y))), 0.5f, 1));
            }
             // If the car is going backwards, add on constant force on these conditions.
             // Driving backward requires a average velocity below 14 and a sideways velocity below 10.
            else if (Mathf.Abs(this.GetComponent<Rigidbody>().velocity.magnitude) < 20.0f && 
                Mathf.Abs(transform.InverseTransformDirection(this.gameObject.GetComponent<Rigidbody>().velocity).x) < 15.0f)
            {  
                body3d.AddForce((-transform.forward * (motorTorque * 0.7f * tractionCoefficient)) * leftTriggerInput);
            }
        }
        #endregion
    }

    private void CalculateDynamicTraction()
    {
        // Adjust the forward and sideways traction accordingly to forward and sideways speed. 
        float adjustedForwardTractionForce = tempForwardTractionForce * forwardTractionCurve.Evaluate(transform.InverseTransformDirection(this.gameObject.GetComponent<Rigidbody>().velocity).z / 70);

        float adjustedSidewaysTractionForce = tempSidewaysTractionForce * sideWaysTractionCurve.Evaluate(Mathf.Abs(transform.InverseTransformDirection(this.gameObject.GetComponent<Rigidbody>().velocity).x / 50));

        // The forward traction force is multiplied by the curve set in the inspector. They are divided by the expected maximum velocity to get a value ranging from 0 to 1. 
        forwardTractionForce = adjustedForwardTractionForce;
        sidewaysTractionForce = adjustedSidewaysTractionForce;
    }

    // Save the input values in a variable every frame.
    private void CacheInput()
    {
        horizontalInput = Device.LeftStickX;
        verticalInput = Device.LeftStickY;
        rightTriggerInput = Device.RightBumper;
        leftTriggerInput = Device.LeftBumper;
        AButton = Device.Action1;
        BButton = Device.Action2;
        XButton = Device.Action3;
        YButton = Device.Action4;
    }

    // Calculate the local rotation every frame.
    private void CalculateLocalYRotation()
    {
        #region Calculate local Y degrees for calculating the difference between cars
        // Get the difference in Y rotation between us.
        if (this.transform.localEulerAngles.y > 180)
        {
            localyDegrees = this.transform.localEulerAngles.y - 360;
        }
        else
        {
            localyDegrees = this.transform.localEulerAngles.y;
        }
        #endregion

        // Store previous and new into variables so we can compare per frame, what our rotation is. 
        lastLocalyDegrees = Mathf.Abs(currentlocalYdegrees);
        currentlocalYdegrees = Mathf.Abs(localyDegrees);
    }

    private void UpdateFMOD()
    {
        // Will use this again when starting on sound design.
        #region Update FMOD RPM data
        // basicEngine_RPM.setValue(body3d.velocity.magnitude * 40);
        // float rpmValue;
        // basicEngine_RPM.getValue(out rpmValue);
        // Debug.Log(rpmValue);
        #endregion
    }

    //-------------------------------------------------------------------------------------------//
    // Methods called from AddSuspensionAndForces().

    // Keeps the car from flipping over and rotating too much on the X and Z axis. 
    private void ConstrainRotation()
    {
        #region Calculate usable localXdegrees and set car to a maximum if its rotated too much
        if (this.transform.localEulerAngles.x > 180)
        {
            localxDegrees = this.transform.localEulerAngles.x - 360;
        }
        else
        {
            localxDegrees = this.transform.localEulerAngles.x;
        }

        // If the car is rotated too much sideways, constrain it.
        if (localxDegrees < -maxAllowedXRotation)
        {
            transform.rotation = Quaternion.Euler(360 - maxAllowedXRotation, this.transform.eulerAngles.y, this.transform.eulerAngles.z);
        }
        if (localxDegrees > maxAllowedXRotation)
        {
            transform.rotation = Quaternion.Euler(maxAllowedXRotation, this.transform.eulerAngles.y, this.transform.eulerAngles.z);
        }
        #endregion

        #region Do the same for the Z axis
        if (this.transform.localEulerAngles.z > 180)
        {
            localzDegrees = this.transform.localEulerAngles.z - 360;
        }
        else
        {
            localzDegrees = this.transform.localEulerAngles.z;
        }

        // If the car is rotated too much forwards, constrain its rotation.
        if (localzDegrees < -maxAllowedZRotation)
        {
            transform.rotation = Quaternion.Euler(this.transform.eulerAngles.x, this.transform.eulerAngles.y, 360 - maxAllowedZRotation);
        }
        // If the car is rotated too much backwards, constrain its rotation.
        if (localzDegrees > maxAllowedZRotation)
        {
            transform.rotation = Quaternion.Euler(this.transform.eulerAngles.x, this.transform.eulerAngles.y, maxAllowedZRotation);
        }

        #endregion
    }

    private void SetTireDataValues()
    {
        #region Set tireData values so the tire itself can give back meaningful values
        // Set wheel position so skidmarks can be drawn there.
        tireData.setWheelPosition(hit.point);
        tireData.Compression = (suspenceLength - hit.distance) / suspenceLength;
        tireData.IsGrounded = true;
        tireData.setWheelVelocity(transform.InverseTransformVector((body3d.GetPointVelocity(raycastOrigin.transform.position))));
        #endregion
    }

    private void ApplyTraction()
    {
        // Don't apply traction when not all wheels are grounded. This prevents the car from spinning around a wheel when only one wheel is grounded.
        // This is not natural behaviour, but it is behaviour needed for the gameplay. 
        if (AllWheelsAreGrounded)
        {
            // Apply traction at every wheel -> We are applying the forces at the raycast origin to keep the car stable.
            // SideWays traction in negative right direction.
            body3d.AddForceAtPosition((-transform.right * tireData.getWheelSidewaysVelocity() * tireData.Compression) * (sidewaysTractionForce * rollingFriction), raycastOrigin.transform.position);
            // Forward traction in negative forward direction.
            body3d.AddForceAtPosition((-transform.forward * tireData.getForwardWheelVelocity() * tireData.Compression) * (forwardTractionForce * rollingFriction), raycastOrigin.transform.position);
        }
    }

    /// <summary>
    /// Sets the friction friction values that are used in the calculation of traction.
    /// Uses the name of the object being hit by a raycast.
    /// </summary>
    /// <param name="hit"></param>
    private void SetFrictionOnSurfaceName(RaycastHit hit)
    {
        if (hit.transform.gameObject.name.Equals("Ice"))
        {
            tractionCoefficient = 0.1f;
            rollingFriction = 0.1f;
        }
        else if (hit.transform.gameObject.name.Equals("Dirt"))
        {
            tractionCoefficient = 0.3f;
            rollingFriction = 3f;
        }
        else if (hit.transform.gameObject.name.Equals("Sand"))
        {
            tractionCoefficient = 0.2f;
            rollingFriction = 7.0f;
        }
        else
        {
            tractionCoefficient = 1.0f;
            rollingFriction = 1.0f;
        }
    }

    /// <summary>
    /// Enables the sphere in the front of the car on a delay so Unity does not make a compound collider of it. 
    /// This would be the case if it would be enabled on awake, or so we think.
    /// </summary>
    /// <param name="seconds"></param>
    /// <returns></returns>
    private IEnumerator EnableSphereColliderOnDelay(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        this.GetComponentInChildren<SphereCollider>().enabled = true;
    }




    //-------------------------------------------------------------------------------------------------------------------//
    // Disable this code when you would like have controller rumble.                                                     //
    // Warning: enabling XInput in InControl has the side effect of Xbox One controllers to show up as two controllers.  //
    // ------------------------------------------------------------------------------------------------------------------//
    
    //private IEnumerator VibrateOnImpact(float duration, float intensity)
    //{
    //    Device.Vibrate(intensity);
    //    yield return new WaitForSeconds(duration);
    //    Device.StopVibration();
    //}

    //-------------------------------------------------------------------------------------------------//
    // Unity in-editor methods. 
    [ContextMenu("SaveCarSettings")]
    private void SaveCarSettings()
    {
#if UNITY_EDITOR
        string localPath = EditorUtility.SaveFilePanelInProject("Save Your ", "InitialName.asset", "asset", "Please select file name to save prefab to:", "Assets/Prefabs/CarSettings/");
        if (!string.IsNullOrEmpty(localPath))
        {
            CarSettings carSettings = ScriptableObject.CreateInstance<CarSettings>();

            carSettings.springConstant = this.springConstant;
            carSettings.dampConstant = this.dampConstant;
            carSettings.motorTorque = this.motorTorque;
            carSettings.steerTorque = this.steerTorque;
            carSettings.suspenceLength = this.suspenceLength;
            carSettings.sidewaysTractionForce = this.sidewaysTractionForce;
            carSettings.forwardTractionForce = this.forwardTractionForce;
            carSettings.maxAllowedXRotation = this.maxAllowedXRotation;
            carSettings.maxAllowedZRotation = this.maxAllowedZRotation;
            carSettings.useCompressionToCalculateTraction = this.useCompressionToCalculateTraction;
            carSettings.forwardTractionCurve = this.forwardTractionCurve;
            carSettings.sidewaysTractionCurve = this.sideWaysTractionCurve;
            carSettings.steerTorqueCurve = this.steeringTorqueCurve;
            AssetDatabase.CreateAsset(carSettings, localPath);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
#endif
    }
}











