﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkidmarkController : MonoBehaviour
{

    // Singleton list to hold all our skids.
    private static List<SkidmarkController> skidmarksList;

    //--------------------------------------------------------------------------
    //Public static method

    /// <summary>
    /// Spawn a skidmark(Gameobject holding a particle trail renderer).
    /// </summary>
    /// <param name="location"></param>
    /// <param name="rotation"></param>
    /// <param name="parent"></param>
    /// <returns></returns>
    public static SkidmarkController Spawn(Vector3 location, Quaternion rotation, Transform parent)
    {
        // Search for the first skidmark that is disabled. 
        foreach (SkidmarkController skidmark in skidmarksList)
        {
            // If disabled, then it's available.
            if (skidmark.gameObject.activeSelf == false)
            {
                // Set it up.
                skidmark.transform.position = location;
                skidmark.transform.SetParent(parent);

                // Set the position to the same as parent.
                skidmark.transform.localPosition = Vector3.zero;

                // Switch it back on so it starts emitting skidmarks.
                skidmark.gameObject.SetActive(true);

                // Return a reference to the caller.
                return skidmark;
            }
        }

        // If we get here, we haven't pooled enough skidmarks.
        // Keep this Debug until we have tested with 8 cars.
        Debug.Log("Not enough skidmarks available in the pool!");
        return null;
    }

    //--------------------------------------------------------------------------
    // Private Monobehavior methods.

    private void Awake()
    {
        // Does the pool exist yet?
        if (skidmarksList == null)
        {
            // Lazy initialize it
            skidmarksList = new List<SkidmarkController>();
        }
        // Add myself
        skidmarksList.Add(this);
    }

    private void OnDestroy()
    {
        // Remove this skidmark from the pool.
        skidmarksList.Remove(this);
        // If this skidmark was the last one in the pool.
        if (skidmarksList.Count == 0)
        {
            // Remove the pool itself.
            skidmarksList = null;
        }
    }


    protected void Start()
    {
        gameObject.SetActive(false);
    }

    // This is called from TireData (Coroutines can't be called!).
    public void SetInActive()
    {
        StartCoroutine(SetInactiveOnDelay(5.0f));
    }


    public IEnumerator SetInactiveOnDelay(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        this.gameObject.SetActive(false);
    }
}



