﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using InControl;

/// <summary>
/// This class takes care of the interactions a player can do with gameObjects. 
/// It handles all the logic of interactions and connecting to objects.
/// </summary>

// Carinteractions depends on these components.
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(BoxCollider))]
[RequireComponent(typeof(InputComponent))]
[RequireComponent(typeof(VehicleAppearanceHandler))]

public class CarInteractions : MonoBehaviour
{
    //---- Public. ----//

    //-- Public getters and setters. --//

    /// <summary>
    /// Get and set the current teamcolor of this racecar.
    /// </summary>
    public TeamColor CurrentTeamColor
    {
        get
        {
            return this.teamColor;
        }
        set
        {
            this.teamColor = value;
            appearanceHandler.SetTeamColorTextures(value);
        }
    }

    public DrivingState GetCurrentDrivingState
    {
        get
        {
            return currentState.GetDrivingState();
        }
    }

    public bool WantsToConnect
    {
        get
        {
            return carWantsToConnect;
        }
        set
        {
            carWantsToConnect = value;
            appearanceHandler.ConnectionLightActive = value;
        }
    }

    public Transform ConnectedCar { get; set; }
    public Transform ConnectedBox { get; set; }
    public RacerType CurrentRacerType { get; private set; }
    public InputDevice Device { get; set; }
    

    //-- Public enums. --//
    public enum DrivingState { Solo = 1, Duo_Pushing = 2, Duo_Steering = 3, CarryingBlock = 4 }
    public enum RacerType { Racer = 1, Blocker = 2, Booster = 3, MultiType = 4 }
    public enum TeamColor { TeamBlue = 1, TeamGreen = 2, TeamRed = 3, TeamPurple = 4, None = 5 }

    //-- Public state variables. --//
    // All there variables need to be public so the states can easily access them.
    // Variables to store connection and disconnection times in. 
    [HideInInspector] 
    public float carConnectTime = 0, carDisconnectTime = 0;
    // Variables used to check if the difference between the time a player dropped a box and picked a box up.
    [HideInInspector] 
    public float boxPickupTime = 0, boxDropTime = 0;
    public GameObject smokeParticle;
    public LayerMask layerToCheckAtConnection;
    public LayerMask carInteractables;

    // Private variables. //
    [SerializeField]
    private TeamColor teamColor;
    [SerializeField]
    private RacerType racerTypeAtAwake;

    private VehicleAppearanceHandler appearanceHandler;
    // Backing field for getter/setter.
    private bool carWantsToConnect;
    
    // Declaration of different states.
    private RaceState soloState;
    private RaceState duoSteeringState;
    private RaceState duoPushingState;
    private RaceState carryingblockState;

    // The current state we are in.
    private RaceState currentState;

    //--------------------------------------------------------------------------------------------//
    // Monobehavior methods (private).
    private void Awake()
    {
        ConnectedCar = null;
        ConnectedBox = null;

        smokeParticle.SetActive(true);
        // Get components.
        appearanceHandler = this.GetComponent<VehicleAppearanceHandler>();
       
        // Declare and initialize all states.
        soloState = this.GetComponent<SoloState>();
        soloState.Initialize(this);
        soloState.StateEnabled = false;

        duoSteeringState = this.GetComponent<DuoSteeringState>();
        duoSteeringState.Initialize(this);
        duoSteeringState.StateEnabled = false;

        duoPushingState = this.GetComponent<DuoPushingState>();
        duoPushingState.Initialize(this);
        duoPushingState.StateEnabled = false;

        carryingblockState = this.GetComponent<CarryingBlockState>();
        carryingblockState.Initialize(this);
        carryingblockState.StateEnabled = false;

        // Racers dont automatically want to connect by default.
        WantsToConnect = false;
        
        // A racer is always starting in a solo drivingstate.
        currentState = soloState;

        //drivingState = DrivingState.Solo;
        CurrentRacerType = racerTypeAtAwake;
    }

    private void Start()
    {
        // Set the cars texture accordingly to the teamcolor.
        CurrentTeamColor = this.teamColor;

        // Always start in solo drivingstate.
        ChangeDrivingState(DrivingState.Solo);
    }

    private void Update()
    {
        currentState.UpdateState(this.transform);
    }

    //------------------------------------------------------------------------------------------------//
    //Public methods. 

    /// <summary>
    /// Changing a cars drivingstate should be done here. 
    /// </summary>
    /// <param name="state"> State to change to. </param>
    public void ChangeDrivingState(DrivingState state)
    {
        RaceState previousState = currentState;
        switch (state)
        {
            case DrivingState.Solo:
                this.currentState = soloState;
                break;

            case DrivingState.Duo_Pushing:
                this.currentState = duoPushingState;
                break;

            case DrivingState.Duo_Steering:
                this.currentState = duoSteeringState;
                break;

            case DrivingState.CarryingBlock:
                this.currentState = carryingblockState;
                break;

            default:
                Debug.LogError("Drivingstate unknown");
                break;
                

        }

        // Enable and disable state enabled flags.
        previousState.StateEnabled = false;
        currentState.StateEnabled = true;

        // Enter the state.
        currentState.EnterState();

        // Change the appeareance of the car depending on the current drivingstate.
        appearanceHandler.ChangeAppearanceOnState(currentState.GetDrivingState());
    }

    /// <summary>
    /// Detaches racecars if they are connected. 
    /// It only has effect when the car is in 'duo pushing' or 'duo steering' driving state.
    /// </summary>
    public void Detach()
    {
        // Call detach on the current drivingstate.
        currentState.Detach();
    }

    /// <summary>
    /// Method thats called from other racecars when they changed something about the connection.
    /// Such as removing the connection or putting themselves in our connection slot.
    /// </summary>
    public void CarConnectionChange()
    {
        // Call CarConnectionChange on the current state.
        currentState.CarConnectionChange();
    }

   

    #region Deprecated methods.
    /// <summary>
    /// Deprecated, please use CurrentTeamColor.
    /// </summary>
    /// <param name="color"></param>
    [System.Obsolete]
    public void SetTeamColor(TeamColor color)
    {
        this.teamColor = color;
        appearanceHandler.SetTeamColorTextures(color);
        Debug.LogWarning("SetTeamColor is deprecated, please change your code to use the CurrentTeamColor get/setter.");
    }
    #endregion 
}