﻿using UnityEngine;
using System.Collections;
using System.Reflection;

public class CarSettings : ScriptableObject
{
    // All public variables from the vehicleController should be stored here.

    public float springConstant;
    public float dampConstant;
    public float motorTorque;
    public float steerTorque;
    public float suspenceLength;
    public float sidewaysTractionForce;
    public float forwardTractionForce;
    public float maxAllowedXRotation;
    public float maxAllowedZRotation;
    public bool useCompressionToCalculateTraction;
    public float currentSpringCompression;
    public Vector3 centerOfMassVec;
    public AnimationCurve sidewaysTractionCurve;
    public AnimationCurve forwardTractionCurve;
    public AnimationCurve steerTorqueCurve;

}
  
