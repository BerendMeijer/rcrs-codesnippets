﻿using UnityEngine;
using System.Collections;

/// <summary>
/// State for the front connected car (the car that is steering).
/// </summary>
public class DuoSteeringState : RaceState
{
    private float motorTorqueAtEnteringState;
    public override void Initialize(CarInteractions interactionsController)
    {
        // Get all components from the interactionsController.
        this.interactionsController = interactionsController;
        this.boxCollider = interactionsController.GetComponent<BoxCollider>();
        this.rigidBody = interactionsController.GetComponent<Rigidbody>();
    }

    public override void EnterState()
    {
        // Save the motortorque in case they change during this state.
        motorTorqueAtEnteringState = interactionsController.GetComponent<VehicleController>().motorTorque;
        Debug.Log("Motor torque at entering state is: " + motorTorqueAtEnteringState);
        // Statements that should only be called once when entering a state.
        boxCollider.size = new Vector3(boxCollider.size.x, boxCollider.size.y, boxCollider.size.z / 2);
    }

    public override void UpdateState(Transform transform)
    {
        this.transform = transform;

        // If the 'A' button is pressed, detach this car from the other.
        if (interactionsController.Device.Action1.WasPressed)
        {
            this.Detach();
        }
    }

    public override void Detach()
    {
        if (interactionsController.ConnectedCar != null)
        {
            // Remove this car from the connected car field of the other car.
            interactionsController.ConnectedCar.GetComponent<CarInteractions>().ConnectedCar = null;

            // Notify the other car that something has changed in its connection.
            interactionsController.ConnectedCar.GetComponent<CarInteractions>().CarConnectionChange();

            // Store the time at which we disconnected.
            interactionsController.carDisconnectTime = Time.time;

            // Set the boxdroptime so cars don't connect immediately to blocks when they disconnect.
            interactionsController.boxDropTime = Time.time;

            // Reset the motor torque to what is was when entering state.
            interactionsController.GetComponent<VehicleController>().motorTorque = motorTorqueAtEnteringState;

            // Give this car a small push when disconnecting.
            this.rigidBody.AddForce(this.transform.forward * 500, ForceMode.Impulse);

            // Rescale boxcollider to its original size.
            this.boxCollider.size = new Vector3(boxCollider.size.x, boxCollider.size.y, boxCollider.size.z * 2);

            this.interactionsController.WantsToConnect = false;

            interactionsController.ConnectedCar = null;

            this.interactionsController.ChangeDrivingState(CarInteractions.DrivingState.Solo);
        }
    }

    public override void CarConnectionChange()
    {
        // If a racecar disconnected, it should have removed itself from this 'ConnectedCar' field.
        if (interactionsController.ConnectedCar == null)
        {
            interactionsController.carDisconnectTime = Time.time;

            // Set the boxdroptime so cars don't connect immediately to blocks when they disconnect.
            interactionsController.boxDropTime = Time.time;

            // Change the box collider to its original length again.
            boxCollider.size = new Vector3(boxCollider.size.x, boxCollider.size.y, boxCollider.size.z * 2);

            interactionsController.ConnectedCar = null;

            Debug.Log("Motor torque at exiting state is: " + motorTorqueAtEnteringState);

            interactionsController.GetComponent<VehicleController>().motorTorque = motorTorqueAtEnteringState;

            this.interactionsController.ChangeDrivingState(CarInteractions.DrivingState.Solo);

            // Give this racecar a small boost when disconnecting.
            rigidBody.AddForce(this.transform.forward * 500, ForceMode.Impulse);
        }
    }

    public override CarInteractions.DrivingState GetDrivingState()
    {
        return CarInteractions.DrivingState.Duo_Steering;
    }


}
