﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// This script is attatched to interactable boxes and can be seen as datacontainers with some small functionality. 
/// Gameobjects set their color and lerp to a default color over time. 
/// </summary>
public class InteractableInfo : MonoBehaviour
{
    // Private variables.
    [SerializeField]
    private int resetColorAfterSeconds;
    private Light interactableLight;
    [SerializeField]
    private LayerMask layerToCheckAtConnecting;
    private Color interactableColor;
    private Renderer localRenderer;
    private BoxCollider localBoxCollider;
    private RaycastHit hit;
    private Rigidbody localRigidbody;
    private Ray ray;
    // Variable used to lerp colors.
    private float time;
    private bool parentedInPlayer = false;
    // References to coroutines so we can stop them.
    private Coroutine removeRigidBodyCoroutine, resetInteractableCoroutine;
    // Public getters and setters.

    /// <summary>
    /// Which team owns this interactable object.
    /// </summary>
    public CarInteractions.TeamColor InteractableClaimedByTeam { get; private set; }

    /// <summary>
    /// Set the parent transform of an interactable object so it can reference the parent easily and attaches to the parent.
    /// </summary>
    public Transform ParentTransform { get; set; }

    /// <summary>
    /// Returns its initial mass at start, taken from the inspector.
    /// </summary>
    /// <returns> </returns>
    public float OriginalMass { get; set; }


    public bool IsParentedInPlayer
    {
        get
        {
            return parentedInPlayer;
        }
        set
        {
            // If this value is changed, update this gameobject. 
            parentedInPlayer = value;
            UpdateOnParentChange();
        }
    }    


    //---------------------------------------------------------------//
    // Public methods.

    /// <summary>
    /// Sets the team that claimed this interactable. The interactable resets itself after a cooldown. 
    /// </summary>
    /// <param name="color"></param>
    public void SetInterActableColorOnTeamColor(CarInteractions.TeamColor color)
    {
        switch (color)
        {
            case CarInteractions.TeamColor.TeamBlue:
                InteractableColor = Color.blue;
                InteractableClaimedByTeam = color;
                interactableLight.color = Color.blue;
                break;
            case CarInteractions.TeamColor.TeamGreen:
                InteractableColor = Color.green;
                InteractableClaimedByTeam = color;
                interactableLight.color = Color.green;
                break;
            case CarInteractions.TeamColor.TeamPurple:
                InteractableColor = Color.magenta;
                InteractableClaimedByTeam = color;
                interactableLight.color = Color.magenta;
                break;
            case CarInteractions.TeamColor.TeamRed:
                InteractableColor = Color.red;
                InteractableClaimedByTeam = color;
                interactableLight.color = Color.red;
                break;
            default:
                Debug.LogError("Color unkown");
                break;
        }
    }


    // Private getters & setters and methods.
    //------------------------------------------------------------------------------------------------//
    // Getters and setters.

    /// <summary>
    /// Setting the interactable team color from another script. 
    /// </summary>
    public Color InteractableColor
    {
        set
        {
            interactableColor = value;
            UpdateColorOnChange();
        }
        get
        {
            return interactableColor;
        }
    }

    //-----------------------------------------------------------------------------------------//

    // ----------------------------------------------------------------------------------------// 
    // Monobehavior methods.
    private void Awake()
    {
        OriginalMass = this.GetComponent<Rigidbody>().mass;
        localRenderer = this.GetComponent<Renderer>();
        localRigidbody = this.GetComponent<Rigidbody>();
        localBoxCollider = this.GetComponent<BoxCollider>();
        IsParentedInPlayer = false;
        interactableLight = this.GetComponent<Light>();
        InteractableClaimedByTeam = CarInteractions.TeamColor.None;
        // At startup, an interactable isnt parented in a player by default.
    }

    //-----------------------------------------------------------------------------------------//

    private void UpdateOnParentChange()
    {
        if (IsParentedInPlayer)
        {
            // Attach when there is enough room behind the player.
            removeRigidBodyCoroutine = StartCoroutine(AttachOnRoomBehindPlayer());
            localBoxCollider.size = new Vector3(0.5f, localBoxCollider.size.y, localBoxCollider.size.z);
        }
        else
        {
            // This gameobject is detached, stop the coroutine if it is still running.
            // The first time an objects gets detached this value will be null.
            if (removeRigidBodyCoroutine != null)
            {
                StopCoroutine(removeRigidBodyCoroutine);
            }
            // Detach from parent.
            this.transform.SetParent(null);
            // Add a rigidbody again so it will react to physics.
            if (!this.gameObject.GetComponent<Rigidbody>())
            {
                this.gameObject.AddComponent<Rigidbody>();
            }
            this.GetComponent<Rigidbody>().mass = OriginalMass;
            localBoxCollider.size = Vector3.one;

        }
    }

    private void UpdateColorOnChange()
    {
        // Stop lerping if we are doing that already.
        if (resetInteractableCoroutine != null)
        {
            StopCoroutine(resetInteractableCoroutine);
        }
        // Reset time so we can use it again to lerp.
        time = 0;
        localRenderer.material.color = InteractableColor;
        resetInteractableCoroutine = StartCoroutine(ResetInteractableColorOnDelay());
    }


    private IEnumerator ResetInteractableColorOnDelay()
    {
        // While the player is holding on to it, wait for it to be released. 
        while (IsParentedInPlayer)
        {
            yield return null;
        }

        yield return new WaitForSeconds(resetColorAfterSeconds);
        // Lerp back to the default color (yellow), so players from other teams know they can pick it up again.

        while (interactableColor != Color.yellow && !IsParentedInPlayer)
        {
            time += Time.deltaTime;
            interactableColor = Color.Lerp(interactableColor, Color.yellow, time / 20);
            interactableLight.color = Color.Lerp(interactableColor, Color.yellow, time / 20);
            localRenderer.material.color = interactableColor;
            yield return null;

        }
        // Reset the claimedby flag so every team can pick it up again.
        InteractableClaimedByTeam = CarInteractions.TeamColor.None;
    }

    /// <summary>
    /// Removes the rigidbody of the interactable when there is enough room behind the player.
    /// </summary>
    /// <returns></returns>
    private IEnumerator AttachOnRoomBehindPlayer()
    {
        // While the specified room isnt available behind the car, don't attach yet, keep yielding in a while loop.
        while (CheckForObjectsBehindCar(4, ParentTransform))
        {
            this.GetComponent<Rigidbody>().mass = 0;
            // Gets called as many times as it can.
            yield return null;
        }
        // Remove the rigidbody so the box does have collision.
        Destroy(this.GetComponent<Rigidbody>());
        // Set this gameobjects transform to right behind the car and reset rotation.
        this.transform.SetParent(ParentTransform);
        this.transform.localRotation = Quaternion.identity;
        this.transform.localPosition = new Vector3(0, 1.5f, -3);
    }

    /// <summary>
    /// Returns true if there is an object obstructing the way.
    /// </summary>
    /// <param name="distance"> Distance in Unity metres. </param> 
    /// <returns></returns>
    public bool CheckForObjectsBehindCar(float distance, Transform parentTransform)
    {
        ray.origin = parentTransform.position;
        ray.direction = -parentTransform.forward;
        // Returns true if there is an object in the way, that is not a box. 
        // The instance ID comparison makes sure that boxes don't glitch in place of this interactable.
        return (Physics.Raycast(ray, out hit, distance) && hit.transform.gameObject.GetInstanceID() != this.gameObject.GetInstanceID());
    }

    /// <summary>
    /// Returns true if the object is not in direct sight of the player; something is obstructing a direct line between the object and the player.
    /// </summary>
    /// <param name="playerToConnectTo"></param>
    /// <returns> True if there is an obstruction. </returns>
    public bool IsInDirectSight(Transform playerToConnectTo)
    {
        // Draw a linecast between this object and the car we want to connect to.
        return (Physics.Linecast(this.transform.position, playerToConnectTo.transform.position, out hit, layerToCheckAtConnecting));
    }
}
