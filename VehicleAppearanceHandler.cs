﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Takes care of changing the appearance of a vehicle, such as texturing and enabling meshes.
/// </summary>
public class VehicleAppearanceHandler : MonoBehaviour 
{
    /// <summary>
    /// Turns on the connection light.
    /// </summary>
    public bool ConnectionLightActive
    {
        get
        {
            return connectionLight.gameObject.activeSelf;
        }
        set
        {
            connectionLight.gameObject.SetActive(value);
        }
    }
    
    [SerializeField]
    private Light connectionLight;
    [SerializeField]
    private Texture[] carRedTextures;
    [SerializeField]
    private Texture[] carBlueTextures;
    [SerializeField]
    private Texture[] carGreenTextures;
    [SerializeField]
    private Texture[] carPurpleTextures;

    // Static int iterators are shared by all vehicleappearancehandlers. 
    // This way, a racecar communicate that a texture variation has already been used.
    // There are always 4 texture variations and one default texture for every teamcolor.
    private static int redVariationIterator = 0;
    private static int blueVariationIterator = 0;
    private static int greenVariationIterator = 0;
    private static int purpleVariationIterator = 0;

    [SerializeField]
    private MeshRenderer carFront, carMiddle, carBack;
    [SerializeField]
    // The gameobject holding the front, middle and back of the car.
    private GameObject meshCollection;

    /// <summary>
    /// Enables and disables meshes based on drivingstate.
    /// </summary>
    /// <param name="state"></param>
    public void ChangeAppearanceOnState(CarInteractions.DrivingState state)
    {
        switch (state)
        {
            case CarInteractions.DrivingState.CarryingBlock:
                break;
            
            case CarInteractions.DrivingState.Duo_Pushing:
                carFront.enabled = false;
                carBack.enabled = true;
                carBack.transform.localPosition = new Vector3(carBack.transform.localPosition.x, carBack.transform.localPosition.y, 0f);
                break;

            case CarInteractions.DrivingState.Duo_Steering:

                // Only use the front mesh of the car that is steering.
                carFront.enabled = true;
                carMiddle.enabled = false;
                carBack.enabled = false;
                // Set the mesh position to match the rescaled box collider.
                carFront.transform.localPosition = new Vector3(carFront.transform.localPosition.x, carFront.transform.localPosition.y, 0.125f);
                break;
            case CarInteractions.DrivingState.Solo:
                carFront.enabled = true;
                carBack.enabled = true;
                carMiddle.enabled = true;
                
                // Set the meshes to the right position. 
                carFront.transform.localPosition = new Vector3(carFront.transform.localPosition.x, carFront.transform.localPosition.y, 0.25f);
                carBack.transform.localPosition = new Vector3(carBack.transform.localPosition.x, carBack.transform.localPosition.y, -0.13f);

                break;
            default:
                Debug.LogError("Unknown drivingstate");
                break;
        }
    }

    /// <summary>
    /// Applies the textures set in the inspector to the car. It also sets the color of the attention light.
    /// </summary>
    public void SetTeamColorTextures(CarInteractions.TeamColor teamcolor)
    {
        // Get a unique texture (this should be done once per car).
        Texture uniqueTexture = GetUniqueTexture(teamcolor);

        // Set the texture to the three meshes that make up the car.
        carFront.material.mainTexture = uniqueTexture;
        carMiddle.material.mainTexture = uniqueTexture;
        carBack.material.mainTexture = uniqueTexture;
        

       switch (teamcolor)
        {
            case CarInteractions.TeamColor.TeamBlue:
                connectionLight.color = Color.blue;
                break;

            case CarInteractions.TeamColor.TeamGreen:
            
                connectionLight.color = Color.green;
                break;

            case CarInteractions.TeamColor.TeamRed:
               
                connectionLight.color = Color.red;
                break;

            case CarInteractions.TeamColor.TeamPurple:
       
                connectionLight.color = Color.magenta;
                break;
            default:
                Debug.LogError("TeamColor unknown");
                break;
        }
    }


    /// <summary>
    ///  Returns a texture variation based on the number of unique textures that are still available for that teamcolor.
    /// </summary>
    /// <param name="teamColor"> The teamcolor you want an unique texture of.</param>
    /// <returns> A texture variation. </returns>
    private Texture GetUniqueTexture(CarInteractions.TeamColor teamColor)
    {
        switch(teamColor)
        {
            case (CarInteractions.TeamColor.TeamRed):

                if(redVariationIterator < carRedTextures.Length - 1)
                {
                    redVariationIterator++;
                    return carRedTextures[redVariationIterator];
                }
                else
                {
                    redVariationIterator = 0;
                    return carRedTextures[redVariationIterator];
                }

            
            case (CarInteractions.TeamColor.TeamBlue):

                if (blueVariationIterator < carBlueTextures.Length - 1)
                {
                    blueVariationIterator++;
                    return carBlueTextures[blueVariationIterator];
                }
                else
                {
                    blueVariationIterator = 0;
                    return carBlueTextures[blueVariationIterator];
                }


            case (CarInteractions.TeamColor.TeamGreen):

                if (greenVariationIterator < carGreenTextures.Length - 1)
                {
                    greenVariationIterator++;
                    return carGreenTextures[greenVariationIterator];
                }
                else
                {
                    greenVariationIterator = 0;
                    return carGreenTextures[greenVariationIterator];
                }

            case (CarInteractions.TeamColor.TeamPurple):
                
                if (purpleVariationIterator < carPurpleTextures.Length - 1)
                {
                    purpleVariationIterator++;
                    return carPurpleTextures[purpleVariationIterator];
                }
                else
                {
                    purpleVariationIterator = 0;
                    return carPurpleTextures[purpleVariationIterator];
                }

            default:
                Debug.LogWarning("TeamColor unknown, returning a default texture.");
                return carRedTextures[0];
                
        }
    }

}
